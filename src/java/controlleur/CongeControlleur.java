/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlleur;

import Util.ModelView;
import annotation.Url;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;
import model.Departement;
import model.Employer;
import service.DeptService;
import service.EmpService;

/**
 *
 * @author Mendrika
 */
public class CongeControlleur {
    private int id;
    private Date debut;
    private Date fin;
    @Url(Url="/liste")
    public ModelView liste()throws Exception{
        ModelView aa=new ModelView();
        HashMap<String,Object> mapper=new HashMap<String, Object>();
        Vector<Employer> dept=EmpService.liste_employer_conge();
        mapper.put("liste", dept);
        
        aa.setMapper(mapper);
        aa.setPage("conge.jsp");
        return aa;
    }
    @Url(Url="/conge")
    public ModelView conge()throws Exception{
        ModelView aa=new ModelView();
        HashMap<String,Object> mapper=new HashMap<String, Object>();
        mapper.put("id", this.id);
        if(this.debut!=null){
            mapper.put("message", "message error");
            EmpService.insert_conge(this.id, this.debut, this.fin);
            mapper.put("message", "message success");
        }
        aa.setMapper(mapper);
        aa.setPage("InsertConge.jsp");
        return aa;
    }
}
