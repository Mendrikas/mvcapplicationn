/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlleur;

import Util.ModelView;
import annotation.Url;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import model.Departement;
import model.Employer;
import service.DeptService;
import service.EmpService;

/**
 *
 * @author Mendrika
 */
public class EmpControlleur {
    private int id;
    private int dept;
    private String nom;
    private String prenom;
    private double salaire;
    @Url(Url="/liste")
    public ModelView lister()throws Exception{
        HashMap<String,Object> mapper=new HashMap<String, Object>();
        Vector<Employer> list=EmpService.liste_employer();
        mapper.put("liste", list);
        ModelView aa=new ModelView();
        aa.setMapper(mapper);
        aa.setPage("liste.jsp");
        return aa;
    }
    @Url(Url="/form")
    public ModelView hashe()throws Exception{
        ModelView aa=new ModelView();
        HashMap<String,Object> mapper=new HashMap<String, Object>();
        Vector<Departement> dept=DeptService.liste_departement();
        mapper.put("dept", dept);
        aa.setMapper(mapper);
        aa.setPage("formulaire.jsp");
        return aa;
    }
    @Url(Url="/insertion")
    public ModelView inserer()throws Exception{
        EmpService.inserer(nom, prenom, salaire);
        ModelView ve=this.lister();
        return ve;
    }

}
