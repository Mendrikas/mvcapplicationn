/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.Date;

/**
 *
 * @author Mendrika
 */
public class Employer {
    private int id;
    private int departement;
    private String nom;
    private String prenom;
    private double salaire;
    private Date date_debut;
    private Date date_fin;
    public Employer(int id, String nom, String prenom, double salaire) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.salaire = salaire;
    }

    public Employer() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDepartement(int departement) {
        this.departement = departement;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public int getDepartement() {
        return departement;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public void setDate_debut(Date date_debut) {
        this.date_debut = date_debut;
    }

    public void setDate_fin(Date date_fin) {
        this.date_fin = date_fin;
    }

    public Employer(String nom, String prenom, double salaire) {
        this.nom = nom;
        this.prenom = prenom;
        this.salaire = salaire;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public double getSalaire() {
        return salaire;
    }
    
    
    
}
