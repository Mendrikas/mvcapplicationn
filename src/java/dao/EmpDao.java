/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;
import model.Employer;

/**
 *
 * @author Mendrika
 */
public class EmpDao {
    public static Vector<Employer> get_employer(Connection con) throws Exception{
        Statement stmt=null;
        Vector<Employer> ve=null;
        try{
            String sql="select * from Employer";
            System.out.println(sql);
            stmt=con.createStatement();
            ResultSet fin=stmt.executeQuery(sql);
            if(fin!=null){
                ve=new Vector<Employer>();
                while(fin.next()){
                    ve.add(new Employer(fin.getInt(1),fin.getString(2),fin.getString(3),fin.getDouble(4)));
                }                
            }
        }
        finally{
            if(con!=null){
                con.close();
            }
            if(stmt!=null){
                stmt.close();
            }
        }
        return ve;
    }
    public static Vector<Employer> get_conge(Connection con) throws Exception{
        Statement stmt=null;
        Vector<Employer> ve=null;
        try{
            String sql="select * from v_conge";
            System.out.println(sql);
            stmt=con.createStatement();
            ResultSet fin=stmt.executeQuery(sql);
            if(fin!=null){
                ve=new Vector<Employer>();
                while(fin.next()){
                    Employer ref=new Employer(fin.getString(1),fin.getString(2),fin.getDouble(3));
                    SimpleDateFormat format=new SimpleDateFormat("yyyy-M-dd");
                    ref.setDate_debut(format.parse(fin.getString(4)));
                    ref.setDate_fin(format.parse(fin.getString(5)));
                    ve.add(ref);
                }                
            }
        }
        finally{
            if(con!=null){
                con.close();
            }
            if(stmt!=null){
                stmt.close();
            }
        }
        return ve;
    }
    public static void insert_conge(Connection con,Employer emp)throws SQLException,Exception{
        PreparedStatement stmt=null;
        SimpleDateFormat format=new SimpleDateFormat("yyyy-M-dd");
        try{
            String don="Insert into Conge(id,date_debut,date_fin) values('"+emp.getId()+"','"+format.format(emp.getDate_debut())+"','"+format.format(emp.getDate_fin())+"')"; 
            
            stmt=con.prepareStatement(don);
            System.out.println(don);
            stmt.executeUpdate();
        }
        finally{
             con.close();            
        }
    }
    public static void insert(Connection con,Employer emp)throws SQLException,Exception{
        PreparedStatement stmt=null;
        try{
            String don="Insert into Employer(nom,prenom,salaire) values('"+emp.getNom()+"','"+emp.getPrenom()+"',"+emp.getSalaire()+")";            
            stmt=con.prepareStatement(don);
            System.out.println(don);
            stmt.executeUpdate();
        }
        finally{
             con.close();            
        }
    }
    
}
