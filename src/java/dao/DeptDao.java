/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import model.Departement;
import model.Employer;

/**
 *
 * @author Mendrika
 */
public class DeptDao {
    public static Vector<Departement> get_dept(Connection con) throws Exception{
        Statement stmt=null;
        Vector<Departement> ve=null;
        try{
            String sql="select * from Departement";
            System.out.println(sql);
            stmt=con.createStatement();
            ResultSet fin=stmt.executeQuery(sql);
            if(fin!=null){
                ve=new Vector<Departement>();
                while(fin.next()){
                    Departement dep=new Departement();
                    dep.setId(fin.getInt(1));
                    dep.setNom(fin.getString(2));
                    ve.add(dep);
                }                
            }
        }
        finally{
            if(con!=null){
                con.close();
            }
            if(stmt!=null){
                stmt.close();
            }
        }
        return ve;
    }
}
