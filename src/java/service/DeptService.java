/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import dao.DeptDao;
import dao.EmpDao;
import java.sql.Connection;
import java.util.Vector;
import model.Departement;
import model.Employer;
import utilitaire.Postgres;

/**
 *
 * @author Mendrika
 */
public class DeptService {
    public static Vector<Departement> liste_departement() throws Exception{
        Connection con=null;
        Vector<Departement> emp=null;
        try{
            con=new Postgres().getConnection();
            emp=DeptDao.get_dept(con);
        }
        finally{
            if(con!=null) con.close();
        }
        return emp;
    }
}
