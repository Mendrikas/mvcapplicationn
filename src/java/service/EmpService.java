/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import dao.EmpDao;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Vector;
import model.Employer;
import utilitaire.Postgres;

/**
 *
 * @author Mendrika
 */
public class EmpService {
    public static Vector<Employer> liste_employer() throws Exception{
        Connection con=null;
        Vector<Employer> emp=null;
        try{
            con=new Postgres().getConnection();
            emp=EmpDao.get_employer(con);
        }
        finally{
            if(con!=null) con.close();
        }
        return emp;
    }
    public static void inserer(String nom,String prenom,double salaire)throws Exception{
        Connection con=null;
        Vector<Employer> emp=null;
        try{
            con=new Postgres().getConnection();
            Employer empp=new Employer(nom,prenom,salaire);
            EmpDao.insert(con, empp);
        }
        finally{
            if(con!=null) con.close();
        }
    }
    public static void insert_conge(int id,Date debut,Date fin)throws Exception{
        Connection con=null;
        Vector<Employer> emp=null;
        try{
            con=new Postgres().getConnection();
            Employer empp=new Employer();
            empp.setId((id));
            empp.setDate_debut((debut));
            empp.setDate_fin((fin));
            EmpDao.insert_conge(con, empp);
        }
        finally{
            if(con!=null) con.close();
        }
    }
    public static Vector<Employer> liste_employer_conge() throws Exception{
        Connection con=null;
        SimpleDateFormat st=new SimpleDateFormat("dd-mm-yyyy");
        LocalDate now=LocalDate.now();
        String[] seq=now.toString().split("-");
        Date nn=new Date(Integer.parseInt(seq[2]),Integer.parseInt(seq[1]),Integer.parseInt(seq[0]));
        Vector<Employer> emp=null;
        try{
            con=new Postgres().getConnection();
            emp=EmpDao.get_conge(con);
            Vector<Employer>filtre=new Vector<Employer>();
            for(int i=0;i<emp.size();i++){
                if(emp.get(i).getDate_debut().compareTo(nn)>0 && emp.get(i).getDate_fin().compareTo(nn)<0){
                    filtre.add(emp.get(i));
                }
            }
        }
        finally{
            if(con!=null) con.close();
        }
        return emp;
    }
}
