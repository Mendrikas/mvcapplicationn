<%-- 
    Document   : formulaire
    Created on : 10 nov. 2022, 20:55:36
    Author     : Mendrika
--%>

<%@page import="model.Departement"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% Vector<Departement> vect=(Vector<Departement>)request.getAttribute("dept"); %>
        <h1>Page Insertion Employer</h1><br>
        <form method="POST" action="insertion.do">
            NOM :
            <input type="text" name="nom"/> <br>
            PRENOM :
            <input type="text" name="prenom"/> <br>
            SALAIRE :
            <input type="text" name="salaire"/> <br>
            Departement :
            <select name="dept">
                <% for(int i=0;i<vect.size();i++) { %>
                <option value=<% out.println(vect.get(i).getId()); %>><% out.println(vect.get(i).getNom()); %></option>
                <% } %>
            </select><br>
            <button>VALIDER</button><br>
            Liste voir <a href=<%=request.getContextPath()+"/DeptControlleur/liste.do" %>>Ici voir la</a>
        </form>
    </body>
</html>
