<%-- 
    Document   : liste
    Created on : 12 nov. 2022, 15:30:36
    Author     : Mendrika
--%>

<%@page import="java.util.Vector"%>
<%@page import="model.Employer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%  %>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    </head>
    <body>
        <h1>Hello World!</h1>
        <% Vector<Employer> emp=(Vector<Employer> )request.getAttribute("liste"); %>
        <table class="table">
            <tr>
                <th>Reference</th>
                <th>Nom</th>
                <th>Salaire</th>
            </tr>
            <% for(int i=0;i<emp.size();i++){%>
            <tr>
                <td><a href=<%=request.getContextPath()+"/CongeControlleur/conge.do?id="+emp.get(i).getId() %>>EMP<%=emp.get(i).getId() %></a></td>
                <td><%=emp.get(i).getNom() %></td>
                <td><%=emp.get(i).getSalaire() %></td>
            </tr>
            <% } %>
        </table><br>
        Formualire insertion <a href=<%=request.getContextPath()+"/MVCApplicationn/EmpControlleur/form.do" %>>Ici voir la</a>
    </body>
</html>
