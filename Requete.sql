CREATE TABLE Employer(
    id serial primary key,
    nom varchar(50),
    prenom varchar(50),
    salaire real,
    idDepartement int  references Departement(id)
);
create table Conge(
    id int  references Employer(id),
    date_debut date,
    date_fin date
);
create table Departement(
    id serial primary key,
    nom varchar(50)
);
insert into Departement(nom) values('Recherche et Développement');
insert into Departement(nom) values('Marketing');
insert into Departement(nom) values('Ventes');
insert into Departement(nom) values('Production');
insert into Departement(nom) values('Comptabilité et Finance');
insert into Departement(nom) values('Ressources humaines');

create view v_conge as select Employer.nom,Employer.prenom,Employer.salaire,Conge.date_debut,Conge.date_fin from Conge inner join Employer on Employer.id=Conge.idemployer; 