<%-- 
    Document   : conge
    Created on : 17 nov. 2022, 10:26:34
    Author     : Mendrika
--%>

<%@page import="java.util.Vector"%>
<%@page import="model.Employer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    </head>
    <body>
        <h1>Liste Employer en Conge :</h1><br>
        <% Vector<Employer> emp=(Vector<Employer> )request.getAttribute("liste"); 
            
            if(emp!=null){
            
        %>
        <table class="table">
            <tr>
                <th>Nom</th>
                <th>Salaire</th>
                <th>Date Debut</th>
                <th>Date Fin</th>
            </tr>
            <% for(int i=0;i<emp.size();i++){%>
            <tr>
                <td><%=emp.get(i).getNom() %></td>
                <td><%=emp.get(i).getSalaire() %></td>
                <td><%=emp.get(i).getDate_debut()%></td>
                <td><%=emp.get(i).getDate_fin()%></td>
            </tr>
            <% }
            } %>
        </table><br>
        Cliquer ici <a href=<%=request.getContextPath()+"/EmpControlleur/liste.do" %>>Voir Employer</a>
    </body>
</html>
