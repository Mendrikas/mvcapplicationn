<%-- 
    Document   : InsertConge
    Created on : 17 nov. 2022, 11:23:31
    Author     : Mendrika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method="POST" action="conge.do">            
            <h1>Insert Conge :</h1><br>
            <input type="hidden" name="id" value=<%=request.getAttribute("id")%>><br>
            Date debut: <input type="date" name="debut"><br>
            Date fin: <input type="date" name="fin"><br>
            <button>Inserer</button>
            <br>
            <% if(request.getAttribute("message")!=null){
                out.println(request.getAttribute("message"));
            }%>
            <a href="/MVCApplicationn/CongeControlleur/liste.do">Par ici</a>
        </form>
    </body>
</html>
